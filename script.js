new Vue({
    el: '#app',
    data: {
      name: '',
      email: '',
      password: '',
      confirmPassword: '',
      submitted: false
    },
    methods: {
      submitForm(event) {
        event.preventDefault();
        // Validar que las contraseñas coincidan
        if (this.password !== this.confirmPassword) {
          alert('Las contraseñas no coinciden');
          return;
        }
        // Después de enviar los datos, puedes mostrar una confirmación o realizar otras acciones
        this.submitted = true;
      }
    }
  });
  